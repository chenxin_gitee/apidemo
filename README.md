# apidemo

#### 介绍
apidemo is an uni-app demo project for api.xlongwei.com

#### 软件架构
uni-app demo project


#### 安装教程

1.  git clone https://gitee.com/xlongwei/apidemo
2.  [https://www.dcloud.io/](https://www.dcloud.io/)，下载HBuilderX
3.  下载微信开发者工具等


#### 使用说明

1.  [apidemo](https://api.xlongwei.com/apidemo/)
2.  [h5](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-h5.png)
3.  [微信小程序](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-app.png)
4.  [安卓APP](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-android.png)


#### 示例功能

1.  [datetime](https://api.xlongwei.com/doku.php?id=api:datetime)：日历、日期、节假日、农历等
2.  [javascript](https://api.xlongwei.com/doku.php?id=api:qn)：问卷问题、带出条件
3.  [validate](https://api.xlongwei.com/doku.php?id=api:validate)：数据校验，证件号、手机号、银行卡号等
4.  [checkcode](https://api.xlongwei.com/doku.php?id=api:checkcode)：验证码，字母、中文、算术、拆字等

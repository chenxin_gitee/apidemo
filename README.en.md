# apidemo

#### Description
apidemo is an uni-app demo project for api.xlongwei.com

#### Software Architecture
uni-app demo project

#### Installation

1.  git clone https://gitee.com/xlongwei/apidemo
2.  [https://www.dcloud.io/](https://www.dcloud.io/) Download HBuilderX


#### Instructions

1.  [apidemo](https://api.xlongwei.com/apidemo/)
2.  [h5](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-h5.png)
3.  [weixin app](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-app.png)
4.  [Android](http://s.xlongwei.com/uploads/img/avatar/weixin-apidemo-android.png)


#### Demo Api

1.  [datetime](https://api.xlongwei.com/doku.php?id=api:datetime): calender, holiday
2.  [javascript](https://api.xlongwei.com/doku.php?id=api:qn): questionnair, condition
3.  [validate](https://api.xlongwei.com/doku.php?id=api:validate): data validation, bankCardNo
4.  [checkcode](https://api.xlongwei.com/doku.php?id=api:checkcode)��checkcode, arithmetic
